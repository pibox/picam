## Synopsis

PiCam is a program that displays a local webcam image on the locally connected display in a PiBox-based system such as the
PiBox Media System.  The program submits a webcam start request to the piboxd daemon which starts an mjpeg-streamer process
on port 9090.  PiCam then starts the omxplayer on the local display and connects it to the localhost port 9090 for display.

PiCam supports the official Raspberry Pi touchscreen display.  Touch actions are defined in the picam.c:imageTouch() function header.

PiCam is based on GTK and Cairo.

## Build

PiCam can be built and tested on a local Linux box using autoconf.  A cross compiled version can be built using the cross.sh wrapper script.

### Local Build and Test

To build locally for testing, use the following commands.

    autoreconf -i
    ./configure
    make
    src/picam -T -v3

The last command will run picam in test mode and use verbosity level three for debugging.  

For more information on command line options use the following command.

    src/picam -?

### Cross compile and packaging

To cross compile the application use the cross.sh script.  To get a simple usage statement for this script use the following command.

    ./cross.sh -?

Use of the cross compile script requires specifying three components.

1. The path to the cross compiler toolchain directory.
1. The path to the PiBox root file system staging directory.
1. The path to the opkg tools on the host system.

The first two components can be found under the respective build trees of the PiBox Development Platform build.  They may also be distributed as part of the packaged PiBox Development Platform.  Either way, the argument for these options is a directory path.

The last component requires installation of the opkg tools on the host.  These can be built from the PiBox Development Platform using the following target.

    make opkg
    make opkg-install

Additional information for building can be found in the INSTALL file.

## Installation

PiCam is packaged in an opkg format.  After cross compiling look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/picam_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/picam_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

