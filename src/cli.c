/*******************************************************************************
 * picam
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <pibox/utils.h>

#include "picam.h"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* -s: Enable TFT mode. */
            case 's':
                cliOptions.flags |= CLI_TFTMODE;
                break;

            /* -T: Use test files. */
            case 'T':
                cliOptions.flags |= CLI_TEST;
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = g_strdup(optarg);
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    PLAYER_T *entry;

    memset(&cliOptions, 0, sizeof(CLI_T));

    /* Get the current VT num so we can change back to it later. */
    cliOptions.vt = (piboxGetVTNum()>0)?piboxGetVTNum():3;
    cliOptions.vttmp = cliOptions.vt + 3;

    cliOptions.url = g_strdup("http://localhost:9090/?action=stream");

    entry = g_new(PLAYER_T, 1);
    memset(entry, 0, sizeof(PLAYER_T));
    entry->formats = g_strdup("default");
    entry->cmd = g_strdup(DEFAULT_PLAYER);
    cliOptions.player = g_slist_append(cliOptions.player, entry);

    /* Are we running as root? */
    if ( getuid() == 0 )
        cliOptions.flags |= CLI_ROOT;
}

/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
    struct stat stat_buf;
    FILE *fd;
    char buf[MAXBUF];
    char *name;
    char *value;
    char *cmd;
    char *ptr;
    char cfg[PATH_MAX];
    PLAYER_T *entry;

    if ( isCLIFlagSet( CLI_TEST) )
        sprintf(cfg, "%s", F_CFG_T);
    else
        sprintf(cfg, "%s", F_CFG);

    // Check that the configuration file exists.
    if ( stat(cfg, &stat_buf) == 0 )
    {
        // Open the config file.
        fd = fopen(cfg, "r");
        if ( fd == NULL )
        {
            fprintf(stderr, "Failed to open %s: %s\n", cfg, strerror(errno));
            return;
        }

        // Each line is name:value pair
        while ( fgets(buf, MAXBUF, fd) != NULL )
        {
            piboxStripNewline(buf);

            name = strtok(buf, ":");
            if ( name == NULL )
                continue;
            fprintf(stderr, "Config file tag: %s\n", name);

            // TFT Mode
            if ( strcasecmp(name, "tft") == 0 )
            {
                cliOptions.flags |= CLI_TFTMODE;
                continue;
            }

            // Set the URL to view
            // This takes the reset of the line as the URL.
            if ( strcmp(name, "url") == 0 )
            {
                ptr = (char *)(buf + strlen(name) + 1);
                if ( cliOptions.url != NULL )
                    g_free(cliOptions.url);
                cliOptions.url = g_strdup(ptr);
                continue;
            }

            /*
             * None of these should have the ":" token in their value.
             */
            value = strtok(NULL, ":");
            if ( value==NULL )
                continue;

            // Set verbosity level
            if ( strcmp(name, "verbose") == 0 )
                cliOptions.verbose = atoi(value);
        
            // Set debug file
            else if ( strcmp(name, "debugfile") == 0 )
            {
                if ( cliOptions.logFile != NULL )
                    g_free(cliOptions.logFile);
                cliOptions.logFile = g_strdup(value);
                cliOptions.flags |= CLI_LOGTOFILE;
            }

            // Set the vt that the X display is on
            else if ( strcmp(name, "vtsrc") == 0 )
            {
                cliOptions.vt = atoi(value);
            }

            // Set the vt that we jump to temporarily 
            // when resetting the display after omxplayer is done.
            else if ( strcmp(name, "vttmp") == 0 )
            {
                cliOptions.vttmp = atoi(value);
            }

            // Set the player command to use.
            // The format of a player entry: 
            // player:<space separated types>:<command>
            else if ( strcmp(name, "player") == 0 )
            {
                cmd= (char *)(value + strlen(value) + 1);
                fprintf(stderr, "format / cmd: %s, %s\n", value, cmd);
                entry = g_new(PLAYER_T, 1);
                memset(entry, 0, sizeof(PLAYER_T));
                entry->formats = g_strdup(value);
                entry->cmd = g_strdup(cmd);
                cliOptions.player = g_slist_append(cliOptions.player, entry);
            }
        }

        fclose(fd);
    }
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*
 *========================================================================
 * Name:   findCommand
 * Prototype:  gint findCommand( gconstpointer )
 *
 * Description:
 * GSList iterator to find the player supporting the specified format.
 *========================================================================
 */
static gint
findCommand( gconstpointer item, gconstpointer user_data )
{
    PLAYER_T *player = (PLAYER_T *)item;
    char *format = (char *)user_data;
    char *str, *ptr;

    str = g_strdup( player->formats );
    ptr = strtok(str, " ");
    while ( ptr != NULL )
    {
        piboxLogger(LOG_INFO, "Testing %s against %s\n", format, ptr);
        if ( strcmp(format, ptr) == 0 )
        {
            g_free(str);
            return 0;
        }
        ptr = strtok(NULL, " ");
    }
    g_free(str);
    return 1;
}

/*========================================================================
 * Name:   findPlayer
 * Prototype:  char *findPlayer( char *format )
 *
 * Description:
 * Find a player that supports the specified format.
 *
 * Arguments:
 * format      The format to search for in the player configurations.
 * 
 * Returns:
 * A pointer to the appropriate command or NULL if the format is not supported.
 * Caller should not free the returned pointer.
 *========================================================================*/
char *
findPlayer( char *format )
{
    GSList *item;
    PLAYER_T *player;

    item = g_slist_find_custom(cliOptions.player, format, (GCompareFunc)findCommand);
    if ( item == NULL )
        return NULL;
    player = item->data;
    return player->cmd;
}

