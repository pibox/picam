/*******************************************************************************
 * picam
 *
 * picam.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PICAM_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "picam.h"

/* Local prototypes */
gboolean idleExit( gpointer arg );

guint homeKey = -1;
GtkWidget *window;

/*
 *========================================================================
 * Name:   picamExit
 * Prototype:  void picamExit( void )
 *
 * Description:
 * Graceful exit of the app.
 *========================================================================
 */
void
picamExit( void )
{
    gtk_main_quit();
}

/*
 *========================================================================
 * Name:   imageTouch
 * Prototype:  void imageTouch( REGION_T *pt )
 *
 * Description:
 * Handler for absolute touch reports.  For PiCam, any touch just means
 * to exit.
 *========================================================================
 */
void
imageTouch( REGION_T *pt )
{
    g_idle_add( (GSourceFunc)idleExit, (gpointer)pt );
}

gboolean
idleExit( gpointer arg )
{
    picamExit();
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    /* Read in /etc/pibox-keysysm */
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        /* Ignore comments */
        if ( buf[0] == '#' )
            continue;

        /* Strip leading white space */
        ptr = buf;
        ptr = piboxTrim(ptr);

        /* Ignore blank lines */
        if ( strlen(ptr) == 0 )
            continue;

        /* Strip newline */
        piboxStripNewline(ptr);

        /* Grab first token */
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        /* Grab second token */
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        /* Set the home key */
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be
 * handled.
 *
 * Notes:
 * Format is
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
    }

    if ( piboxGetDisplayTouch() )
    {
        setCLIFlag(CLI_TOUCH);
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    piboxLogger(LOG_INFO, "Key press event caught.\n");
    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                gtk_main_quit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            gtk_main_quit();
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
    g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 2048, 1024);
    gtk_window_set_title(GTK_WINDOW(window), "VideoFE");

    return window;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 *
 * Notes:
 * This is a gtk program in order to properly capture keyboard input events
 * that stop the playback from the webcam.
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    uuid_t  uuid;
    char    tag[37];
    char    gtkrc[1024];
    char    edid[256];
    FILE    *fd;
    struct stat stat_buf;

    GtkWidget *window;

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }
    piboxLogger(LOG_INFO, "vtsrc: %d\n", cliOptions.vt);
    piboxLogger(LOG_INFO, "vttmp: %d\n", cliOptions.vttmp);
    piboxLogger(LOG_INFO, "url  : %s\n", cliOptions.url);

    /* Read environment config for keyboard behaviour */
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();

    /*
     * If we're on a touchscreen, register the input handler.
     */
    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(imageTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }

    /*
     * Check for EDID config.
     */
    if ( stat(EDID_F, &stat_buf) == 0 )
    {
        fd = fopen(EDID_F, "r");
        if ( fd )
        {
            if ( fscanf(fd, "%s", edid) != EOF )
            {
                edidCmd = (char *)calloc(1, strlen(EDID_RESET_P) + strlen(edid) + 2);
                sprintf(edidCmd, EDID_RESET_P, edid);
                if ( stat(edidCmd, &stat_buf) == 0 )
                {
                    piboxLogger(LOG_INFO, "edidCmd: %s\n", edidCmd);
                    setCLIFlag( CLI_EDIDMODE );
                }
                else
                {
                    free(edidCmd);
                    edidCmd = NULL;
                    piboxLogger(LOG_INFO, "No such file: %s\n", edidCmd);
                }
            }
            else
                piboxLogger(LOG_ERROR, "Failed to read EDID file: %s\n", strerror(errno));
            fclose(fd);
        }
        else
            piboxLogger(LOG_ERROR, "Failed to open EDID file: %s\n", strerror(errno));
    }

    /* Notify piboxd to start mjpeg-streamer */
    uuid_generate(uuid);
    uuid_unparse_upper(uuid, tag);
    piboxMsg(MT_STREAM, MA_START, 0, tag, strlen("webcam"), (gchar *)"webcam", NULL, NULL);

    gtk_init(&argc, &argv);
    if ( isCLIFlagSet( CLI_TEST) )
        gtk_rc_parse(gtkrc);
    else
        piboxLogger(LOG_INFO, "CLI_TEST is not set.\n");
    window = createWindow();
    gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);

    /* Stop any previous processes */
    shutdownPlayerProcessor();

    /* piboxMsg API doesn't tell us if mjpeg-streamer started.  So wait for it here. */
    if ( waitForStreamer() != 0 )
    {
        piboxLogger(LOG_ERROR, "Failed to find stream to play.\n");
        goto bail;
    }

    /* Start the webcam in a subprocess. */
    startPlayerProcessor(cliOptions.url);

    gtk_main();

bail:
    shutdownPlayerProcessor();
    piboxMsg(MT_STREAM, MA_END, 0, tag, strlen("webcam"), (gchar *)"webcam", NULL, NULL);

    if ( isCLIFlagSet( CLI_TOUCH ) )
    {
        piboxLogger(LOG_INFO, "Shutting down touch handler.\n");
        /* SIGINT is required to exit the touch processor thread. */
        raise(SIGINT);
        piboxTouchShutdownProcessor();
    }

    piboxLogger(LOG_INFO, "Exiting.\n");
    piboxLoggerShutdown();
    return 0;
}

